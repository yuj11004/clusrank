TO DO LIST
============

## 02/23/2018
1. Go through the reviews, leave initial answers to questions that can be
   answered now.
2. For Reviewer 1, remark 1, carry out a simulation for N(0, 1) vs N(0, 2) to
   check the power of the 
3. For Reviewer 1, remark 3, add t-dist and gamma to data generating function. Carry
   out simulation study using these two functions and put the results in the
   package vignette. Also add data generating option for creating ties.
4. For Reviewer 1, remark 4, put simulation result for comparison between
   methods into the appendix.  
5. For Reviewer 2, clarify the difference between ClusterRankTest, their
   code has minor mistakes (if they haven't updated yet). They have one more
   function for informative sample size.
6. For Reviewer 2, the package is designed to have one function to make it
   user friendly. 
7. Find the reference for invariant to monotonic transformations of the
   data.
8. Check the index generation for the package, it seems there is some
   problem , see Reviewers last comment.
