# To-do #
## Oct 04, 2016,
+ Add the functions in the paper to the testrun.R



## July 26, 2016, 11:00 am
+ ~~Go through the formulas~~
+ ~~Every notation needs to be justified.~~
+ ~~obs?~~
+ ~~See if all cluster size equals 1, what will happen to DS?~~
+ ~~name of the function~~.
+ For the exact method, when cluster size equals 1, add comment to man.
+ Exact method, the trick, summary of algorithm. Put it in appendix.
+ ~~Reduce the number of things in the printout.~~
+ ~~10 clusters each with size 5, the group correlation parameter.~~
+ ~~People using RGL and DS method with different signs, inorder to get the same sign just change the group based on which the statistic is calculated.~~
+ ~~Combine two data sets to test m-group rank sum test.~~
+ ~~Extra space in the powersim function. "ds" ,"rgl"~~
+ ~~Use cid instead of id.~~
+ ~~Demonstrate strata~~
+ ~~Remove the set.seed. Put set.seed in a separate chunk.~~
+ ~~Explain simulation function. dleta = 0, delta = 0.5,~~
+ Let's use this simulation funcion for one scneario, repeately use this function for different configuration to generate the table. When the group level is individual, hasn't been compared in the literation. Focus on what haven't been done.
+ ~~Look at how they assign treatment to individual level group.~~
+ ~~keep in the datgen funtions.~~
+ ~~Emprical rejection rate, of the two clustered rank sum test under different scenarios.~~
+ Highlight easy simluation study tool.
+ Colatteral data doesn't work, DS. Write in the paper.
+ ~~Add AR1 as another correlation structure. binarySimCLF, construct AR1.~~
+ multiplication use parenthesis.
+ Use empirical percentage instead of rate.
+ Uppder bound of SE.
+ Multivariate signed-rank test. Can possibily add it to the package. One paragraph needs to be applied with care. For example, if the RGL method if the data correlation is not exchangeable, but AR1, what will happen.
+ ~~cite mvtnorm, citations of packages.~~

## August 9, 2016, 9:15 am
+ ~~Choose the cpp functions to be exported.~~
+ ~~Clean up the C++ code. No space between parenthesis and first variable.~~
+ ~~Change the text for clusgrp.~~
+ d.c
+ ~~Zc -> Z~~
+ Added that these two tests are the same. Mention the test.
+ ~~Change score to x.~~
+ Define M as maximum of ni.
+ ~~Table: Exchangeable -> ex~~
+ ~~Don't connect lines.~~ Add to current table.
+ Discuss result and simulation setting in two paragraphs.
+ Talk about size, then power, power is affacted by sample size, effect size, ....
+ rho 0.9 -> 0.5. 0.1 -> (0.1, 0.9). Highlight undone simulation in the abstract.
+ Illustrate simpower, 2 paragraphs.
+ highlight the unbalanced.
+ signed-rank test, delta = 0.1, 0.3.
+ ~~Missing rate. <- keep rate.~~
+ Citation Laroque. one book chapter and one biometrica.
+ ~~semicolumn after score. Fix printout.~~

